/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Auto;
import ec.edu.espol.model.Camion;
import ec.edu.espol.model.Camioneta;
import ec.edu.espol.model.Motocicleta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.proyectopoo2p.App;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class RegistrarVehiculoFXMLController implements Initializable {


    @FXML
    private TextField marca;
    @FXML
    private ComboBox tipo;
    @FXML
    private TextField modelo;
    @FXML
    private TextField motor;
    @FXML
    private TextField año;
    @FXML
    private TextField color;
    @FXML
    private TextField combustible;
    @FXML
    private TextField transmision;
    @FXML
    private TextField precio;
    @FXML
    private HBox vidrioBox;
    @FXML
    private TextField placa;
    @FXML
    private TextField recorrido;
    @FXML
    private Text ruta;
    private Persona p = Persona.extraerpersonatemporal();
    private Vehiculo v=null;
    int n=0;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> tipos = new ArrayList<>();
        tipos.add("Motocicleta");
        tipos.add("Auto");
        tipos.add("Camión");
        tipos.add("Camioneta");
        tipo.setItems(FXCollections.observableArrayList(tipos));
        Text v=new Text("Dependiendo de el vehiculo a ingresar se le permitira ingresar este dato");
        restricciones();
    }   
    private void restricciones(){
        marca.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        motor.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        color.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        combustible.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        transmision.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        precio.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[0-9 .]+"))? change:null));
        año.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[0-9 .]+"))? change:null));
        recorrido.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[0-9 .]+"))? change:null));
    }
    String t="default";
    TextField vidrios=new TextField();
    @FXML
    private void tipoVehiculo(ActionEvent e){
        vidrioBox.getChildren().clear();
        ComboBox cbx = (ComboBox) e.getSource();
        t = (String) cbx.getValue();
        if(t.equals("Auto")||t.equals("Camión")||t.equals("Camioneta")){
            vidrioBox.getChildren().add(vidrios);
         }
        
    }
    @FXML
    private void registrarVehiculo(MouseEvent event) {
        if(t.equals("default")){
            Alert a2=new Alert(Alert.AlertType.WARNING,"Seleccione el tipo de vehiculo");
            a2.show();
        }else if(marca.getText().isEmpty()||modelo.getText().isEmpty()||motor.getText().isEmpty()||año.getText().isEmpty()||placa.getText().isEmpty()||recorrido.getText().isEmpty()||combustible.getText().isEmpty()||transmision.getText().isEmpty()||precio.getText().isEmpty()||color.getText().isEmpty()||ruta.getText().isEmpty()){
            Alert a2=new Alert(Alert.AlertType.WARNING,"No dejar campos vacíos y adjuntar una imagen.");
            a2.show();
        }else if(!t.equals("Motocicleta")&&vidrios.getText().isEmpty()){
            Alert a2=new Alert(Alert.AlertType.WARNING,"No dejar campos vacíos.");
            a2.show();
        }
        else if(Vehiculo.existe(placa.getText())){
            Alert a=new Alert(Alert.AlertType.WARNING,"Este vehículo se encuentra registrado.");
            a.show();
        }
        else if(t.equals("Camión")){
            v=new Camion(placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(año.getText()),Double.parseDouble(recorrido.getText()),color.getText(),combustible.getText(),transmision.getText(),vidrios.getText(),Double.parseDouble(precio.getText()),ruta.getText());
            v.serializarvehiculo(p);
            Alert a=new Alert(Alert.AlertType.INFORMATION,"REGISTRADO CON ÉXITO");
            a.show();
        }else if(t.equals("Camioneta")){
            v=new Camioneta(placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(año.getText()),Double.parseDouble(recorrido.getText()),color.getText(),combustible.getText(),transmision.getText(),vidrios.getText(),Double.parseDouble(precio.getText()),ruta.getText());
            v.serializarvehiculo(p);
            Alert a=new Alert(Alert.AlertType.INFORMATION,"REGISTRADO CON ÉXITO");
            a.show();
        }else if(t.equals("Auto")){
            v=new Auto(placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(año.getText()),Double.parseDouble(recorrido.getText()),color.getText(),combustible.getText(),transmision.getText(),vidrios.getText(),Double.parseDouble(precio.getText()),ruta.getText());
            v.serializarvehiculo(p);
            Alert a=new Alert(Alert.AlertType.INFORMATION,"REGISTRADO CON ÉXITO");
            a.show();
        }else if(t.equals("Motocicleta")){
            v=new Motocicleta(placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(año.getText()),Double.parseDouble(recorrido.getText()),color.getText(),combustible.getText(),transmision.getText(),Double.parseDouble(precio.getText()),ruta.getText());
            v.serializarvehiculo(p);
            Alert a=new Alert(Alert.AlertType.INFORMATION,"REGISTRADO CON ÉXITO");
            a.show();}
    }
    @FXML
    private void adjuntarImg(MouseEvent event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new ExtensionFilter ("JPG Files" , "*.jpg"));
        File imagenVehiculo = fc.showOpenDialog(null);
        try{
            if(imagenVehiculo!=null){
                ruta.setText(imagenVehiculo.getAbsolutePath());
            }
        }catch(Exception ex){
            Alert a=new Alert(Alert.AlertType.ERROR,"Error");
            a.show();
        } 
    }

    @FXML
    private void volver(MouseEvent event) throws IOException {
        if(n==0){
            App.setRoot("VendedorFXML");
        }else
            App.setRoot("AmbosFXML");
        
    }
    public void setN(int i){
        n=i;
    }
}
