/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Cuenta;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class TipoFXMLController implements Initializable {


    @FXML
    private Button comprador;
    @FXML
    private Button vendedor;
    @FXML
    private Button volver;
    @FXML
    private Button perfil;
    Cuenta cuenta;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void compradores(MouseEvent event)  {
        try {
            App.setRoot("CompradorFXML");
        } catch (IOException ex) {
        }
    }

    @FXML
    private void vendedores(MouseEvent event) {
        try {
            App.setRoot("VendedorFXML");
        } catch (IOException ex) {
        }
    }
    @FXML
    private void ambos(MouseEvent event) {
        try {
            App.setRoot("AmbosFXML");
        } catch (IOException ex) {
        }
    }

    @FXML
    private void volver(MouseEvent event) {
        try {
            App.setRoot("LoginFXML");
        } catch (IOException ex) {
        }
    }
    @FXML
    private void verperfil(MouseEvent event)  {
        try {
            App.setRoot("PerfilFXML");
        } catch (IOException ex) {
        }
    }
}
