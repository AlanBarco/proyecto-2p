/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;


import ec.edu.espol.model.Auto;
import ec.edu.espol.model.Camion;
import ec.edu.espol.model.Camioneta;
import ec.edu.espol.model.Motocicleta;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.proyectopoo2p.App;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import javafx.scene.text.Text;
import javax.mail.*;
import javax.mail.internet.*;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class AceptarOfertaFXMLController implements Initializable {


    @FXML
    private Text correoComprador;
    @FXML
    private Text precioOfertado;
    @FXML
    private Text cantidadOfertas;
    @FXML
    private Text modelo;
    @FXML
    private Text numeroOferta;
    @FXML
    private Text preciooficial;
    @FXML
    private VBox imagenes;
    private ArrayList<Oferta> ofertas =Oferta.deserealizarofertas("ofertastempo.ser");
    /**
     * Initializes the controller class.
     */
    private int n= 0;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ofertas.sort(Oferta::compareTo);
        Oferta of=ofertas.get(0);
        mostrarOferta(n);
        modelo.setText(of.getVehiculo().getmodelo());
        preciooficial.setText(String.valueOf(of.getVehiculo().getprecio()));
        try{
            FileInputStream f=new FileInputStream(ofertas.get(0).getVehiculo().getimagen());
            Image im=new Image(f);
            ImageView iv=new ImageView(im);            
            iv.setFitWidth(331);
            iv.setFitHeight(313);
            imagenes.getChildren().clear();
            imagenes.getChildren().add(iv);
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
        }
        
    }    
    @FXML
    private void siguienteOferta(ActionEvent event) { 
        if(n==ofertas.size()-1){
            Alert a=new Alert(Alert.AlertType.INFORMATION,"No puede avanzar mas");
            a.show();
        }else{
            n+=1;
            mostrarOferta(n);
        }
        
    }
    @FXML
    private void aceptarOferta(ActionEvent event) {
        Properties properties = new Properties();
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.port","587");			                
        properties.put("mail.smtp.auth", "true");        
        Session session = Session.getDefaultInstance(properties,new javax.mail.Authenticator() {           
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(properties.getProperty("datv41039@gmail.com"),properties.getProperty("datv2013"));
            }				
        });        
        try{
            Properties prop = new Properties();
            InputStream inputStream = new FileInputStream(new File("correo.properties"));
            prop.load(inputStream);
            String placa=ofertas.get(n).getVehiculo().getplaca();
            String modelo=ofertas.get(n).getVehiculo().getmodelo();
            String tipo="default";
            if(ofertas.get(n).getVehiculo() instanceof Auto)
                tipo="del auto";
            else if(ofertas.get(n).getVehiculo() instanceof Camion)
                tipo="del camion";
            else if(ofertas.get(n).getVehiculo() instanceof Camioneta)
                tipo="de la camioneta";
            else if(ofertas.get(n).getVehiculo() instanceof Motocicleta)
                tipo="de la motocicleta";
            Double precio=ofertas.get(n).getPrecio();
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(ofertas.get(n).getComprador().getCorreoelectronico()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(ofertas.get(n).getComprador().getCorreoelectronico()));
            message.setSubject("Oferta aceptada");
            message.setText("Se le comunica que su oferta "+tipo+" de placa "+placa +" por "+precio+" ha sido aceptada por el vendedor "+Persona.extraerpersonatemporal().getNombres()+" "+Persona.extraerpersonatemporal().getApellidos()+", se adjunta la infomacion detallada del vehiculo que adquirio:\n"+ofertas.get(n).getVehiculo().toString());
            Transport t = session.getTransport("smtp");            
            t.connect(prop.getProperty("correo"),prop.getProperty("clave"));                
            t.sendMessage(message,message.getAllRecipients());
            Alert a=new Alert(Alert.AlertType.INFORMATION,"Oferta aceptada con exito, se ha enviado un correo con su verificación");
            a.show();
            t.close();
            ofertas.get(n).getVehiculo().eliminarvehiculo();
        }catch (MessagingException me){                        
            Alert a=new Alert(Alert.AlertType.INFORMATION,"Hubo problemas al enlazar el servidor con el correo,intente con otro");
            a.show();                                          
        }catch(IOException e){
            Alert c=new Alert(Alert.AlertType.WARNING,"No se pudo leer la informacin del correo de la concesionaria");
            c.show();
        }
        
    }

    @FXML
    private void anteriorOferta(ActionEvent event) {
        if(n==0){
            Alert a=new Alert(Alert.AlertType.INFORMATION,"No puede retroceder mas");
            a.show();
        }else{
            n-=1;
            mostrarOferta(n);
        }
        
    }
    private void mostrarOferta(int i){
        Oferta of=ofertas.get(i);
        correoComprador.setText(of.getComprador().getCorreoelectronico());
        precioOfertado.setText(String.valueOf(of.getPrecio()));
        if(ofertas.size()!=1)
            cantidadOfertas.setText("Tiene: " + String.valueOf(ofertas.size()) + " ofertas");
        else
            cantidadOfertas.setText("Tiene: " + String.valueOf(ofertas.size()) + " oferta");
        numeroOferta.setText("Oferta " + (i+1));
        
    }
    @FXML
    private void volver(ActionEvent event) throws IOException {
        App.setRoot("verOfertasFXML");
    }
}
