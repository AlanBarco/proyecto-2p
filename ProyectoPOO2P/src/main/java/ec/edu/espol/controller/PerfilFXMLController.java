/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class PerfilFXMLController implements Initializable {


    @FXML
    private Text nombres;
    @FXML
    private Text apellidos;
    @FXML
    private Text correo;
    @FXML
    private Text organizacion;
    @FXML
    private Text usuario;
    @FXML
    private Text clave;
    
    private Cuenta cuenta;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Persona p=Persona.extraerpersonatemporal();
        nombres.setText(p.getNombres());
        apellidos.setText(p.getApellidos());
        correo.setText(p.getCorreoelectronico());
        organizacion.setText(p.getOrganizacion());
        usuario.setText(p.getUsuario());
        clave.setText(p.getClave());
    }    
    
    @FXML
    private void cambiarClave(MouseEvent event) throws IOException {
        App.setRoot("CambiarContrasenaFXML");
    }
    
    @FXML
    private void volver(MouseEvent event) throws IOException {
        App.setRoot("TipoFXML");
    }
}
