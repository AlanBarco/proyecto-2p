/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Parametros;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.proyectopoo2p.App;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author alan_
 */
public class OfertarVehiculoFXMLController implements Initializable {


    @FXML
    private Text marca;
    @FXML
    private Text modelo;
    @FXML
    private Text placa;
    @FXML
    private Text precio;
    @FXML
    private Text numero;
    @FXML
    private Text vehiculon;
    @FXML
    private VBox imagenes;
    private ArrayList<Vehiculo> v=new ArrayList<>();
    private int n=0;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb){
        v=Vehiculo.deserializarvehiculos("vehitemporales.ser");
        int i=v.size();
        numero.setText("Vehículos"+"\n"+"disponibles:"+i);
        mostrarvehiculo(n);        ;
    }
    
    @FXML
    private void anterior(MouseEvent event) {
        if(v.size()!=0){
            if(n==0){
                Alert a=new Alert(AlertType.INFORMATION,"No puede retroceder más");
                a.show();
            }else{
                n-=1;
                mostrarvehiculo(n);
            }
        }
    }

    @FXML
    private void volver(MouseEvent event) throws IOException {
        try {
                App.setRoot("ParametrosFXML");
            }catch (IOException ex) {
            }
    }
    @FXML
    private void ofertar(MouseEvent e) throws IOException{
        FXMLLoader fxml=App.loadFXMLload("hacerOfertaFXML");
        App.setRoot(fxml);
        HacerOfertaFXMLController c=fxml.getController();
        c.setM(n);
        ///
    }
    @FXML
    private void siguiente(MouseEvent e){
        if(v.size()!=0){    
            if(n==v.size()-1){
                Alert a=new Alert(AlertType.INFORMATION,"No puede avanzar más");
                a.show();
            }else{
                n+=1;
                mostrarvehiculo(n);
            }
        }
    }
    @FXML
    private void precioa1(MouseEvent e){
        v.sort(Vehiculo::compareTo);
        n=0;
        mostrarvehiculo(n);
    }
    @FXML
    private void preciod1(MouseEvent e){
        v.sort(Vehiculo::compareTo);
        n=0;
        v=Vehiculo.invertir(v);
        mostrarvehiculo(n);
    }
    @FXML
    private void añoa1(MouseEvent e){
        Vehiculo.ordenarañoascendente(v);
        n=0;
        mostrarvehiculo(n);
    }
    @FXML
    private void añod1(MouseEvent e){
        Vehiculo.ordenarañoascendente(v);
        v=Vehiculo.invertir(v);
        n=0;
        mostrarvehiculo(n);
    }
    public void ObtenerParametros(Parametros parametro,ArrayList<String>num){
        v = parametro.buscaroferta(num);
        System.out.println(v);
    }
    private void mostrarvehiculo(int i){
        imagenes.getChildren().clear();
        Vehiculo ve=v.get(i);
        marca.setText(ve.getmarca());
        modelo.setText(ve.getmodelo());
        placa.setText(ve.getplaca());
        precio.setText(String.valueOf(ve.getprecio()));
        vehiculon.setText("Vehículo "+(i+1));
        try{
            FileInputStream f=new FileInputStream(ve.getimagen());
            Image im=new Image(f);
            ImageView iv=new ImageView(im);            
            iv.setFitWidth(354);
            iv.setFitHeight(243);
            imagenes.getChildren().clear();
            imagenes.getChildren().add(iv);
        }catch(FileNotFoundException e){
        }
    }
    public int getN(){
        return n;
    }
    public void setN(int i){
    n=i;
    }
}
