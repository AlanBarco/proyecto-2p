/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Util;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class HacerOfertaFXMLController implements Initializable {


    @FXML
    private TextField precioOfertado;
    private int m=0;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        precioOfertado.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[0-9 .]+"))? change:null));
    }    
    
    @FXML
    private void volver(MouseEvent event) throws IOException {
        App.setRoot("OfertarVehiculoFXML");
        
    }

    @FXML
    private void ofertar(MouseEvent event) throws IOException {
        if(!precioOfertado.getText().isEmpty()){
            Vehiculo v=Vehiculo.deserializarvehiculos("vehitemporales.ser").get(m);
            Persona comprador=Persona.extraerpersonatemporal();
            comprador.setClave(Util.codigohash(comprador.getClave()));
            Persona vendedor=v.pertenecea();
            Oferta o=new Oferta(comprador,Double.parseDouble(precioOfertado.getText()),v);
            o.guardaroferta();
            Alert a=new Alert(AlertType.CONFIRMATION,"Oferta Realizada.");
            a.show();
        }else{
            Alert a=new Alert(AlertType.WARNING,"No ha ofertado ningun precio.");
            a.show();
        }
    }
    public void setM(int i){
        m=i;
    }

}
