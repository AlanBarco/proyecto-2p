/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author alan_
 */
public class AmbosFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void ingresarVehiculo(ActionEvent event) throws IOException {
        FXMLLoader fxml=App.loadFXMLload("RegistrarVehiculoFXML");
        App.setRoot(fxml);
        RegistrarVehiculoFXMLController c=fxml.getController();
        c.setN(1);
    }

    @FXML
    private void volver(ActionEvent event) throws IOException {
        App.setRoot("TipoFXML");
    }

    @FXML
    private void verOfertas(ActionEvent event) throws IOException {
        FXMLLoader fxml=App.loadFXMLload("verOfertasFXML");
        App.setRoot(fxml);
        VerOfertasFXMLController c=fxml.getController();
        c.setN(1);
    }

    @FXML
    private void ofertarVehiculo(ActionEvent event) throws IOException {
        FXMLLoader fxml=App.loadFXMLload("ParametrosFXML");
        App.setRoot(fxml);
        ParametrosFXMLController c=fxml.getController();
        c.setN(1);
    }

}
