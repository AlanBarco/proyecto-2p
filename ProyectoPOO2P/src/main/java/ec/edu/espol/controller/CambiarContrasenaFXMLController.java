/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Persona;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author familia torres vera
 */
public class CambiarContrasenaFXMLController implements Initializable {


    @FXML
    private PasswordField pass;
    @FXML
    private PasswordField newPass;
    @FXML
    private PasswordField confNPss;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {             
    }    
    
    @FXML
    private void volver(MouseEvent event) throws IOException {
        App.setRoot("LoginFXML");
    }

    @FXML
    private void cambiar(MouseEvent event)throws IOException  {
        Persona p=Persona.extraerpersonatemporal();                      
        if(pass.getText().isEmpty()||newPass.getText().isEmpty()||confNPss.getText().isEmpty()){
            Alert a=new Alert(Alert.AlertType.WARNING,"No dejar campos vacíos.");
            a.show();
        }
        else if((!p.getClave().equals(pass.getText()))){
            Alert a=new Alert(Alert.AlertType.WARNING,"Contraseña anterior incorrecta");
            a.show();
        }
        else if((p.getClave().equals(newPass.getText()))){
            Alert a=new Alert(Alert.AlertType.WARNING,"La nueva contraseña no puede ser igual a la contraseña anterior");
            a.show();
        }
        else if(!newPass.getText().equals(confNPss.getText())){
            Alert a=new Alert(Alert.AlertType.WARNING,"La confirmacion de la contraseña no es igual a la nueva contraseña");
            a.show();
        }
        else{            
            p.cambiarClave(newPass.getText());            
            Alert a=new Alert(Alert.AlertType.INFORMATION,"Cambio de contraseña con éxito.");
            a.show();
            
        }
        
    }

}
