/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Persona;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class CompradorFXMLController implements Initializable {
    private Persona persona;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void ofertarVehiculo(MouseEvent event) throws IOException {        
        try {
                App.setRoot("ParametrosFXML");
            }catch (IOException ex) {
            }
    }

    @FXML
    private void verPefil(MouseEvent event) throws IOException {
        App.setRoot("PerfilFXML");
    }

    @FXML
    private void volver(MouseEvent event) throws IOException {
        App.setRoot("TipoFXML");
    }


}
