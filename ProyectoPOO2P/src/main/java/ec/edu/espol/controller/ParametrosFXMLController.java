/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Parametros;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class ParametrosFXMLController implements Initializable {


    @FXML
    private CheckBox recorridoCheck;
    @FXML
    private CheckBox añoCheck;
    @FXML
    private CheckBox precioCheck;
    @FXML
    private CheckBox tipoCheck;
    @FXML
    private ComboBox tipoVehiculo=new ComboBox();
    @FXML
    private HBox recorridoPane;
    @FXML
    private HBox añoPane;
    @FXML
    private HBox precioPane;
    @FXML
    private VBox tipoPane;
    private Parametros p=new Parametros();;
    private Text t=new Text("Se buscaran todas las posibilidades"+"\n"+ "de los parametros no seleeccionado");
    private String tipo="default";
    private int n=0;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> tipos = new ArrayList<>();
        tipos.add("Motocicleta");
        tipos.add("Auto");
        tipos.add("Camión");
        tipos.add("Camioneta");
        tipoVehiculo.setItems(FXCollections.observableArrayList(tipos));
        tipoPane.getChildren().add(t);    
        tipoVehiculo.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent t) {
                ComboBox cbx = (ComboBox) t.getSource();
                tipo=(String) cbx.getValue();
            }
        });
    } 
    private ArrayList<String> listaParametros(){
        ArrayList<String> num = new ArrayList<>();
        if(tipoCheck.isSelected())
            num.add("1");
        if(recorridoCheck.isSelected())
            num.add("2");
        if(añoCheck.isSelected())
            num.add("3");
        if(precioCheck.isSelected())
            num.add("4");
        return num;
    }
    @FXML
    private void tipoCheck(MouseEvent event) throws IOException {
        tipoPane.getChildren().clear();
        if(tipoCheck.isSelected()){
            tipoVehiculo.setCenterShape(true);
            tipoPane.getChildren().add(tipoVehiculo);
        }
        else
            tipoPane.getChildren().add(t);
    }
    TextField recorrido1;
    TextField recorrido2;
    @FXML
    private void recorridoCheck(MouseEvent event) {
        recorridoPane.getChildren().clear();
        if(recorridoCheck.isSelected()){
            recorrido1 = new TextField();
            recorrido2 = new TextField();
            recorrido1.setTextFormatter(new TextFormatter<>(change ->(change.getControlNewText().matches("[0-9 .]+"))? change:null));
            recorrido2.setTextFormatter(new TextFormatter<>(change ->(change.getControlNewText().matches("[0-9 .]+"))? change:null));
            recorridoPane.getChildren().add(recorrido1);
            recorridoPane.getChildren().add(recorrido2);
        }
    }
    TextField año1;
    TextField año2;
    @FXML
    private void añoCheck(MouseEvent event) {
        añoPane.getChildren().clear();
        if(añoCheck.isSelected()){
            año1 = new TextField();
            año2 = new TextField();
            año1.setTextFormatter(new TextFormatter<>(change ->(change.getControlNewText().matches("[0-9]+"))? change:null));
            año2.setTextFormatter(new TextFormatter<>(change ->(change.getControlNewText().matches("[0-9]+"))? change:null));
            añoPane.getChildren().add(año1);
            añoPane.getChildren().add(año2);
        }
    }
    TextField precio1;
    TextField precio2;
    @FXML
    private void precioCheck(MouseEvent event) {
        precioPane.getChildren().clear();
        if(precioCheck.isSelected()){
            precio1 = new TextField();
            precio2 = new TextField();
            precio1.setTextFormatter(new TextFormatter<>(change ->(change.getControlNewText().matches("[0-9 .]+"))? change:null));
            precio2.setTextFormatter(new TextFormatter<>(change ->(change.getControlNewText().matches("[0-9 .]+"))? change:null));
            precioPane.getChildren().add(precio1);
            precioPane.getChildren().add(precio2);
        }
    }
    private void arrayRecorrido(){
        if(recorrido1.getText().isEmpty()||recorrido2.getText().isEmpty()){
            Alert a=new Alert(Alert.AlertType.WARNING,"NO deje campos vacios");
        }
        else{
            double r1=Double.parseDouble(recorrido1.getText());
            double r2=Double.parseDouble(recorrido2.getText());
            double[] s=new double[2];
            if(r1<r2){
                s[0]=r1;
                s[1]=r2;
            }
            else{
                s[0]=r2;
                s[1]=r1;
            }
            p.setRecorrido(s);
        }
    }
    private void arrayAño(){
        if(año1.getText().isEmpty()||año2.getText().isEmpty()){
            Alert a=new Alert(Alert.AlertType.WARNING,"NO deje campos vacios");
        }
        else{
            int a1=Integer.parseInt(año1.getText());
            int a2=Integer.parseInt(año2.getText());
            int[] s=new int[2];
            if(a1<a2){
                s[0]=a1;
                s[1]=a2;
            }
            else{
                s[0]=a2;
                s[1]=a1;
            }
            p.setAño(s);
        }
    }

    private void arrayPrecio(){
        if(precio1.getText().isEmpty()||precio2.getText().isEmpty()){
            Alert a=new Alert(Alert.AlertType.WARNING,"NO deje campos vacios");
        }
        else{
            double p1=Double.parseDouble(precio1.getText());
            double p2=Double.parseDouble(precio2.getText());
            double[] s=new double[2];
            if(p1<p2){
                s[0]=p1;
                s[1]=p2;
            }
            else{
                s[0]=p2;
                s[1]=p1;
            }
            p.setPrecio(s);
        }
    }
    private void validar(){
        String s="No deje campos vacios";
        String s1="Debe seleccionar un tipo de vehiculos";
        String s2="No debe dejar campos vacios y debe elegir un tipo de vehiculo";
        int n=0;
        int m=0;
        Alert a=new Alert(Alert.AlertType.WARNING,s);
        a.setContentText(tipo);
        for(Node txt:añoPane.getChildren()){
            TextField txt2=(TextField)txt;
            if(txt2.getText().isEmpty()){
                n+=1;
            }
        }
        for(Node txt:precioPane.getChildren()){
            TextField txt2=(TextField)txt;
            if(txt2.getText().isEmpty()){
                n+=1;
                break;
            }
        }
        for(Node txt:recorridoPane.getChildren()){
            TextField txt2=(TextField)txt;
            if(txt2.getText().isEmpty()){
                n+=1;
                break;
            }
        }
        if(tipoCheck.isSelected()&&tipo.equals("default")&&n!=0){
            a.setContentText(s2);
            a.show();
        }else if(n!=0){
            a.setContentText(s);
            a.show();
        }else if(tipoCheck.isSelected()&&tipo.equals("default")){
            a.setContentText(s1);
            a.show();
        }
            
    }
    @FXML
    private void volver(ActionEvent event) throws IOException {
        if(n==0)
            App.setRoot("CompradorFXML");
        else
            App.setRoot("AmbosFXML");
    }
    @FXML
    private void buscar(MouseEvent event) throws IOException {
        validar();
        if(tipoCheck.isSelected())
            p.setTipo(tipo);
        if(recorridoCheck.isSelected())
            arrayRecorrido();
        if(añoCheck.isSelected())
            arrayAño();
        if(precioCheck.isSelected())
            arrayPrecio();
        ArrayList<String> numeros=listaParametros();
        ArrayList<Vehiculo> vehiculos=p.buscaroferta(numeros);
        ArrayList<Vehiculo> vehiculos2=Vehiculo.deserializarvehiculos("vehiculos.ser");
        ArrayList<Vehiculo> vehidisponibles=Vehiculo.filtrar(vehiculos,Persona.extraerpersonatemporal().getUsuario());
        if(vehidisponibles.size()!=0){
            Vehiculo.serializarlistavehiculos(vehidisponibles,"vehitemporales.ser");
            App.setRoot("OfertarVehiculoFXML");
        }else if(vehiculos2.size()!=0&&vehiculos.size()==0){
            Alert a=new Alert(AlertType.INFORMATION,"No hay vehiculos que cumplan con los parametro que solicita");
            a.show();
        }else {
            Alert a=new Alert(AlertType.INFORMATION,"Aun no hay disponibles para la venta");
            a.show();
        }
    }
    public void setN(int i){
        n=i;
    }
}
