/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class VerOfertasFXMLController implements Initializable {


    @FXML
    private TextField placa;
    @FXML
    private Button Volver;
    @FXML
    private Button verOfertas;
    private int n=0;
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }  
    @FXML
    private void verOfertas(ActionEvent event) throws IOException {
        Persona p = Persona.extraerpersonatemporal(); 
        if(placa.getText().isEmpty()){
            Alert a=new Alert(Alert.AlertType.WARNING,"Campo vacío.");
            a.show(); 
        }
        else if(!Vehiculo.existe(placa.getText())){
            Alert a=new Alert(Alert.AlertType.WARNING,"Vehículo no registrado.");
            a.show(); 
        }
       else if(p.tiene(placa.getText())!=1){
            int n=p.tiene(placa.getText());
            if(n==2){
                Alert a=new Alert(Alert.AlertType.WARNING,"No es el dueño del vehiculo.");
                a.show();   
            }
            if(n==3){
                Alert a=new Alert(Alert.AlertType.WARNING,"Este vehiculo no esta registrado");
                a.show();   
            }
        }   
        else{
            ArrayList<Oferta> ofertas=Oferta.leerOfertas(placa.getText());
            if(ofertas.size()!=0){
                try{
                     Oferta.serializarlistaofertas(ofertas,"ofertastempo.ser");
                     App.setRoot("AceptarOfertaFXML");
                 }catch(IOException ex){}
            }else{
                Alert a=new Alert(Alert.AlertType.WARNING,"No ha recibido ofertas de este vehiculo.");
                a.show();
            }
        }
    }
    
    @FXML
    private void volver(ActionEvent event) throws IOException {
        if(n==0)
            App.setRoot("VendedorFXML");
        else
            App.setRoot("AmbosFXML");
    }
    public void setN(int i){
        n=i;
    }
}
