/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Util;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author USER
 */
public class RegistroFXMLController implements Initializable {


    @FXML
    private TextField nombres;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField correo;
    @FXML
    private TextField organizacion;
    @FXML
    private TextField user;
    @FXML
    private TextField clave;
    @FXML
    private Button registrarse2;
    @FXML
    private Button volver;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nombres.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        apellidos.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
    }    
    
    @FXML
    private void volver(MouseEvent event) {
        try {
               App.setRoot("LoginFXML");
           } catch (IOException ex) {
               Alert a=new Alert(Alert.AlertType.ERROR,"Error");
                a.show();
           }
    }
    @FXML
    private void registrar(MouseEvent event) {
        Cuenta cuenta =new Cuenta(user.getText(),Util.codigohash(clave.getText()));
        Persona p=new Persona(nombres.getText(),apellidos.getText(),correo.getText(),organizacion.getText(),cuenta);
        if(nombres.getText().isEmpty()||apellidos.getText().isEmpty()||correo.getText().isEmpty()||organizacion.getText().isEmpty()||user.getText().isEmpty()||clave.getText().isEmpty()){
            Alert a2=new Alert(Alert.AlertType.WARNING,"No dejar campos vacíos.");
            a2.show();
        }
        else if(p.existeusuario() == 3){
            Alert a=new Alert(Alert.AlertType.WARNING,"Usuario y correo electrónico ya existente.");
            a.show();
        }
        else if(p.existeusuario() == 2){
            Alert a=new Alert(Alert.AlertType.WARNING,"Correo electrónico ya existente.");
            a.show();
        }
        else if(p.existeusuario() == 1){
            Alert a=new Alert(Alert.AlertType.WARNING,"Nombre de Usuario ya existente.");
            a.show();
        }
        else{
            p.registrarusuario();
            Alert a=new Alert(Alert.AlertType.INFORMATION,"Registro con éxito.");
            a.show();
        }
    }
}
