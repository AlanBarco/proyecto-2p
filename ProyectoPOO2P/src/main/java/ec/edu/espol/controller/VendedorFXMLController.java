/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author alan_
 */
public class VendedorFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    private void ingresarVehiculo(MouseEvent event) throws IOException {
        App.setRoot("RegistrarVehiculoFXML");
    }
    @FXML
    private void verOfertas(MouseEvent event) throws IOException {
        App.setRoot("verOfertasFXML");
    }
    @FXML
    private void volver(MouseEvent event) throws IOException {
        App.setRoot("TipoFXML");
    }

}
