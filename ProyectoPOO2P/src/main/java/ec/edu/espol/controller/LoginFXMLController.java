/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Util;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.proyectopoo2p.App;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author familia torres vera
 */
public class LoginFXMLController implements Initializable {


    @FXML
    private TextField usuario;
    @FXML
    private PasswordField clave;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void registrarse(MouseEvent event) {
        try {
               App.setRoot("RegistroFXML");
            } catch (IOException ex) {
            }
        ArrayList<Vehiculo> v = Vehiculo.deserializarvehiculos("vehiculos.ser");
    }

    @FXML
    private void login(MouseEvent event) {
        Cuenta cuenta=new Cuenta(usuario.getText(),Util.codigohash(clave.getText()));
        if(usuario.getText().isEmpty()||clave.getText().isEmpty()){
                Alert a=new Alert(Alert.AlertType.WARNING,"No dejar campos vacíos.");
                a.show();
        }
        else if(cuenta.login()==1){
            try {
                Persona p=cuenta.extraerusuario();
                p.setClave(clave.getText());
                p.guardarpersonatemporal();
                App.setRoot("TipoFXML");
            } catch (IOException ex) {
            }
        }
        else{
            if(cuenta.login()==2){
                Alert a=new Alert(AlertType.INFORMATION,"Clave incorrecta.");
                a.show();
            }if(cuenta.login()==3){
                Alert a=new Alert(AlertType.INFORMATION,"Usuario no registrado.");
                a.show();
            }
        }
    }

}
