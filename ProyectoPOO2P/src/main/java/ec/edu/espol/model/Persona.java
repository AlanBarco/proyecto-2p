/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * @author alan_
 */
public  class Persona implements Serializable{
    private String nombres;
    private String apellidos;
    private String correoelectronico;
    private String organizacion;
    private Cuenta cuenta;
    private static final long serialVersionUID = 456846846564L;
    public Persona(String nombres,String apellidos,String correoelectronico,String organizacion,Cuenta cuenta){
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.correoelectronico=correoelectronico;
        this.organizacion=organizacion;
        this.cuenta=cuenta;
    }
    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCorreoelectronico() {
        return correoelectronico;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public String getUsuario() {
        return cuenta.getUsuario();
    }

    public String getClave() {
        return cuenta.getClave();
    }
    public Cuenta getCuenta() {
        return cuenta;
    }
    public void setClave(String s){
        this.cuenta.setClave(s);
    }
    @Override
    public String toString(){ 
            return(this.nombres+","+this.apellidos+","+this.correoelectronico+","+this.organizacion+","+this.cuenta);
    }
    public int existeusuario(){
        try(FileInputStream f=new FileInputStream("usuarios.ser");ObjectInputStream in=new ObjectInputStream(f)){
            ArrayList<Persona> personas=(ArrayList<Persona>)in.readObject();
            for(Persona p:personas){
                if(this.getUsuario().equals(p.getUsuario())&&this.correoelectronico.equals(p.correoelectronico))
                    return 3;
                if(this.getUsuario().equals(p.getUsuario()))
                    return 1;
                if(this.correoelectronico.equals(p.correoelectronico))
                    return 2;                   
            }
        }
        catch(Exception e){
        }
        return 4;
    }
        
    public int tiene(String p){
        HashMap<String,ArrayList<String>> registro=Util.extraerregistro();
        for(HashMap.Entry<String,ArrayList<String>> r:registro.entrySet()){
            if(r.getValue().contains(p)&&r.getKey().equals(this.getUsuario()))
                return 1;
            if(r.getValue().contains(p))
                return 2;
        }
        return 3;
    }
    public static ArrayList<Persona> extraerusuarios(){
        ArrayList<Persona> personas=new ArrayList<>();
        try(FileInputStream f=new FileInputStream("usuarios.ser");ObjectInputStream in=new ObjectInputStream(f)){
            personas=(ArrayList<Persona>)in.readObject();
        }
        catch (Exception ex) { 
        }
        return personas;
    }
    public static void guardarlistausuarios(ArrayList<Persona> personas){
         try(FileOutputStream f=new FileOutputStream("usuarios.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(personas);
        }
        catch (IOException ex) { 
        }
    }
    public void registrarusuario(){
        ArrayList<Persona> personas=Persona.extraerusuarios();
        personas.add(this);
        Persona.guardarlistausuarios(personas);
        HashMap<String,ArrayList<String>> registros=Util.extraerregistro();
        registros.put(this.getUsuario(),new ArrayList<String>());
        Util.guardarregistro(registros);
    }
    public static Persona extraerpersonatemporal(){
        Persona p=null;
        try(FileInputStream f=new FileInputStream("ptemporal.ser");ObjectInputStream in=new ObjectInputStream(f)){
            p=(Persona)in.readObject();
        }
        catch (IOException ex) { 
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
        }
        return p;
    }
    public void guardarpersonatemporal(){
        try(FileOutputStream f=new FileOutputStream("ptemporal.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(this);
        }
        catch (IOException ex) { 
        }
    }
    public static Persona extraerusuario(String user){
        ArrayList<Persona> p=Persona.extraerusuarios();
        Persona p1=null;
        for(Persona pe:p){
            if(pe.getUsuario().equals(user)){
                p1=pe;
            }
        }
        return p1;
    }
    public void cambiarClave(String clave){
        try(FileInputStream f=new FileInputStream("usuarios.ser");ObjectInputStream in=new ObjectInputStream(f)){
            ArrayList<Persona> personas=(ArrayList<Persona>)in.readObject();
            ArrayList<Persona> persons=new ArrayList<>();  
            for(Persona p:personas){                
                if(!p.getUsuario().equals(this.getUsuario()))
                    persons.add(p);                   
                else{
                    p.setClave(Util.codigohash(clave));
                    persons.add(p);
                }
            } 
            Persona.guardarlistausuarios(persons);
            
        }catch(Exception e){
        }
    }
}
