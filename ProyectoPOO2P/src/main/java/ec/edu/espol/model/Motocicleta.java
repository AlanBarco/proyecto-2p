
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;


/**
 *
 * @author alan_
 */
public class Motocicleta extends Vehiculo{
    
    public Motocicleta(String placa, String marca, String modelo, String motor, int año, double recorrido, String color, String combustible, String transmision, double precio,String imagen){
        super(placa, marca, modelo, motor, año, recorrido, color, combustible, transmision, precio,imagen);
    }
    public Motocicleta(){}
}
