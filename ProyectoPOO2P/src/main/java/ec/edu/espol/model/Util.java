/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author alan_
 */
public class Util implements Serializable{
    private static final long serialVersionUID = 123846564L;
    
    
    private Util(){
    }
    public static boolean isdouble(String s){
        try{
            Double.parseDouble(s);
            return true;
        }
        catch(NumberFormatException e){
            return false;
        }
    }
    public static byte[] getSHA(String input) throws NoSuchAlgorithmException 
    { 
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        return md.digest(input.getBytes(StandardCharsets.UTF_8));  
    }
    public static String toHexString(byte[] hash) 
    { 
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexString = new StringBuilder(number.toString(16)); 
        while (hexString.length() < 32)  
        {  
            hexString.insert(0, '0');  
        }  
        return hexString.toString();  
    }
    public static String codigohash(String s){
        try {
            return toHexString(getSHA(s));
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }
    public static HashMap<String,ArrayList<String>>  extraerregistro(){
        HashMap<String,ArrayList<String>> registros=new HashMap<>();
        try(FileInputStream f=new FileInputStream("registros.ser");ObjectInputStream in=new ObjectInputStream(f)){
            registros=(HashMap<String,ArrayList<String>>)in.readObject();
        }
        catch (IOException ex) { 
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
        }
        return registros;
    }
    public static void guardarregistro(HashMap<String,ArrayList<String>> registros){
        try(FileOutputStream f=new FileOutputStream("registros.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(registros);           
        }
        catch (IOException ex) { 
        }
    }
    public static void limpiararchivos(){ 
        ArrayList<Persona> pe=new ArrayList<>();
        ArrayList<Oferta> of=new ArrayList<>();
        ArrayList<Vehiculo> ve=new ArrayList<>();
        HashMap<String,ArrayList<String>> re=new HashMap<>();
        try(FileOutputStream f=new FileOutputStream("usuarios.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(pe);
        }
        catch (IOException ex) { 
        }
        try(FileOutputStream f=new FileOutputStream("ofertas.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(of);
        }
        catch (IOException ex) { 
        }
        try(FileOutputStream f=new FileOutputStream("vehiculos.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(ve);
        }
        catch (IOException ex) { 
        }
        try(FileOutputStream f=new FileOutputStream("registros.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(re);
        }
        catch (IOException ex) { 
        }
    }
}
