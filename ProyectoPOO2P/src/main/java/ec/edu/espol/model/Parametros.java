/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.ArrayList;

/**
 *
 * @author USER
 */
public class Parametros {
    private String tipo="default";
    private double[] recorrido=new double[2];
    private int[] año=new int[2];
    private double[] precio=new double[2];

    public Parametros(String tipo,double[] recorrido,int[] año,double[] precio) {
        this.tipo = tipo;
        this.recorrido=recorrido;
        this.año=año;
        this.precio=precio;
    }
    public Parametros(){
        
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setRecorrido(double[] recorrido) {
        this.recorrido = recorrido;
    }

    public void setAño(int[] año) {
        this.año = año;
    }

    public void setPrecio(double[] precio) {
        this.precio = precio;
    }
    
    public ArrayList<Vehiculo> buscaroferta(ArrayList<String> num){
        ArrayList<Vehiculo> vehiculos=Vehiculo.deserializarvehiculos("vehiculos.ser");
        Vehiculo ve=null;
        ArrayList<Vehiculo> ofertas=new ArrayList<>();
        if(this.tipo.equals("Auto"))
            ve=new Auto();
        else if(this.tipo.equals("Camión"))
            ve=new Camion();
        else if(this.tipo.equals("Camioneta"))
            ve=new Camioneta();
        else if(this.tipo.equals("Motocicleta"))
            ve=new Motocicleta();
        double rec1=this.recorrido[0];
        double rec2=this.recorrido[1];
        int año1=this.año[0];
        int año2=this.año[1];
        double prec1=this.precio[0];
        double prec2=this.precio[1];
        for(Vehiculo v:vehiculos){
            int n=0;
            if(!tipo.equals("default")&&num.contains("1")&&v.getClass()==ve.getClass())
                n+=1;
            if(num.contains("2")&&(v.getrecorrido()>=rec1&&v.getrecorrido()<=rec2))
                n+=1;
            if(num.contains("3")&&(v.getaño()>=año1&&v.getaño()<=año2))
                n+=1;
            if(num.contains("4")&&(v.getprecio()>=prec1&&v.getprecio()<=prec2))
                n+=1;
            if(n==num.size())
                ofertas.add(v);
        }
        return ofertas;
    }

    @Override
    public String toString() {
        return  tipo  + ","+recorrido[0] +","+ recorrido[1] +","+año[0] +","+año[1]+ ","+ precio[0]+","+precio[1] ;
    }
}
