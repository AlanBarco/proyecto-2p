/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author USER
 */
public class Cuenta implements Serializable{
    private String usuario;
    private String clave;
    private static final long serialVersionUID = 123846564L;

    public Cuenta(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }
    public void setClave(String s){
        this.clave=s;
    }
    @Override
    public String toString() {
        return usuario+clave;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cuenta other = (Cuenta) obj;
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.clave, other.clave)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.usuario);
        hash = 79 * hash + Objects.hashCode(this.clave);
        return hash;
    }
    public int login(){
        try(FileInputStream f=new FileInputStream("usuarios.ser");ObjectInputStream in=new ObjectInputStream(f)){
            ArrayList<Persona> personas=(ArrayList<Persona>)in.readObject();
            for(Persona p:personas){
                if(p.getCuenta().equals(this))
                    return 1;
                if(p.getUsuario().equals(this.getUsuario()))
                    return 2;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return 3;
    }
    public Persona extraerusuario(){
        try(FileInputStream f=new FileInputStream("usuarios.ser");ObjectInputStream in=new ObjectInputStream(f)){
            ArrayList<Persona> personas=(ArrayList<Persona>)in.readObject();
            for(Persona p:personas){
                if(p.getCuenta().equals(this))
                    return p;                
            }
        }catch(Exception e){
        }
        return null;
    }        
}       
