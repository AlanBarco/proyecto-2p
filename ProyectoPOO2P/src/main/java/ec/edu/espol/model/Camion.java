
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;


/**
 *
 * @author alan_
 */
public class Camion extends Vehiculo{
    private String vidrios;
    
    public Camion(String placa, String marca, String modelo, String motor, int año, double recorrido, String color, String combustible, String transmision, String vidrios, double precio,String imagen){
        super(placa, marca, modelo, motor, año, recorrido, color, combustible, transmision, precio,imagen);
        this.vidrios = vidrios;
    }
    public Camion(){}

    public String getVidrios() {
        return vidrios;
    }

    public void setVidrios(String vidrios) {
        this.vidrios = vidrios;
    }


}
