/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author USER
 */
public class Oferta implements Serializable,Comparable<Oferta>{
    private static final long serialVersionUID = 5932143937137871286L;
    private Persona comprador;
    private double precio;
    private Vehiculo vehiculo;
    
    public Oferta(Persona comprador,double precio,Vehiculo vehiculo) {
        this.comprador = comprador;
        this.precio=precio;
        this.vehiculo=vehiculo;
    }
    
    public double getPrecio() {
        return precio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }
    
    public String getCorreo(){
        return comprador.getCorreoelectronico();
    }

    public Persona getComprador() {
        return comprador;
    }
    public static ArrayList<Oferta> deserealizarofertas(){
        ArrayList<Oferta> ofertas=new ArrayList<>();
        try(FileInputStream f=new FileInputStream("ofertas.ser");ObjectInputStream in=new ObjectInputStream(f)){
           ofertas=(ArrayList<Oferta>)in.readObject();
        }
        catch (IOException | ClassNotFoundException ex) {
        }
        return ofertas;
    }
    public static ArrayList<Oferta> deserealizarofertas(String archivo){
        ArrayList<Oferta> ofertas=new ArrayList<>();
        try(FileInputStream f=new FileInputStream(archivo);ObjectInputStream in=new ObjectInputStream(f)){
           ofertas=(ArrayList<Oferta>)in.readObject();
        }
        catch (IOException | ClassNotFoundException ex) {
        }
        return ofertas;
    }

    public static void serializarlistaofertas(ArrayList<Oferta> ofertas){
        try(FileOutputStream f=new FileOutputStream("ofertas.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(ofertas);
        }
        catch (IOException ex) { 
        }
    }
    public static void serializarlistaofertas(ArrayList<Oferta> ofertas, String archivo){
        try(FileOutputStream f=new FileOutputStream(archivo);ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(ofertas);
        }
        catch (IOException ex) { 
        }
    }


    public void guardaroferta(){
        ArrayList<Oferta> ofertas=Oferta.deserealizarofertas();
        ofertas.add(this);
        Oferta.serializarlistaofertas(ofertas);
    }
    public static ArrayList<Oferta> leerOfertas(String placa){
        ArrayList<Oferta> ofertasvalidas=new ArrayList<>();
        ArrayList<Oferta> ofertas=Oferta.deserealizarofertas();
        for(Oferta o:ofertas){
            if(o.getVehiculo().getplaca().equals(placa))
                ofertasvalidas.add(o);
        }
        return ofertasvalidas;
    }

    @Override
    public String toString() {
        return "Oferta{" + "comprador=" + this.comprador + ", precio=" + this.precio + ", vehiculo=" + this.vehiculo + '}';
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Oferta other = (Oferta) obj;
        return Objects.equals(this.vehiculo, other.vehiculo);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.comprador);
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.precio) ^ (Double.doubleToLongBits(this.precio) >>> 32));
        hash = 83 * hash + Objects.hashCode(this.vehiculo);
        return hash;
    }

    @Override
    public int compareTo(Oferta o) {
        if(o.getPrecio()<this.getPrecio())
            return -1;
        else if(o.getPrecio()>this.getPrecio()){
            return 1;
        }else
            return 0;
    }

   
}
        

