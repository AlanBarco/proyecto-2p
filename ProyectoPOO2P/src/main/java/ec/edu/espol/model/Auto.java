
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author alan_
 */
public class Auto extends Vehiculo implements Serializable{
    private static final long serialVersionUID = 6529685098267757690L;
    private String vidrios;
    public Auto(){}
    public Auto(String placa, String marca, String modelo, String motor, int año, double recorrido, String color, String combustible, String transmision,String vidrios, double precio,String imagen){
        super(placa, marca, modelo, motor, año, recorrido, color, combustible, transmision, precio,imagen);
        this.vidrios = vidrios;
        
    }
    public Auto(String placa){
        super(placa);
    }

    public String getVidrios() {
        return vidrios;
    }

    public void setVidrios(String vidrios) {
        this.vidrios = vidrios;
    }
}


