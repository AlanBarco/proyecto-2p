/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 *
 * @author alan_
 */
public class Vehiculo implements Serializable,Comparable<Vehiculo>{
    private static final long serialVersionUID = 12312312L;
    protected String placa;
    protected String marca;
    protected String modelo;
    protected String motor;
    protected int año;
    protected double recorrido;
    protected String color;
    protected String combustible;
    protected String transmision;
    protected double precio;
    protected Persona vendedor;
    protected String imagen;
    public Vehiculo(){}
    public Vehiculo(String placa, String marca, String modelo, String motor, int año, double recorrido, String color, String combustible, String transmision, double precio,String imagen){
              //COLOCAR VALIDACION DE PLACA CON EL SISTEMA
            this.placa = placa;
            this.marca = marca;
            this.modelo = modelo;
            this.motor=motor;
            this.año = año;
            this.recorrido = recorrido;
            this.color = color;
            this.combustible = combustible;
            this.transmision = transmision;
            this.precio = precio;
            this.imagen=imagen;
               
    }
    public Vehiculo(String placa){
        this.placa = placa;
    }

    public String getplaca() {
        return placa;
    }

    public String getmarca() {
        return marca;
    }

    public String getmodelo() {
        return modelo;
    }

    public String getmotor() {
        return motor;
    }

    public int getaño() {
        return año;
    }

    public double getrecorrido() {
        return recorrido;
    }

    public String getcolor() {
        return color;
    }

    public String getcombustible() {
        return combustible;
    }

    public String gettransmision() {
        return transmision;
    }

    public double getprecio() {
        return precio;
    }
    public String getimagen() {
        return imagen;
    }
    public static void serializarlistavehiculos(ArrayList<Vehiculo> vehiculos,String nomfile){
        try(FileOutputStream f=new FileOutputStream(nomfile);ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(vehiculos);
        }
        catch (IOException ex) { 
        }
    }
    public static ArrayList<Vehiculo> deserializarvehiculos(String nomfile){
        ArrayList<Vehiculo> vehiculos=new ArrayList<>();
        try(FileInputStream f=new FileInputStream(nomfile);ObjectInputStream in=new ObjectInputStream(f)){
           vehiculos=(ArrayList<Vehiculo>)in.readObject();
        }
        catch (IOException | ClassNotFoundException ex) {
        }
        return vehiculos;
    }
    
    public void serializarvehiculo(Persona p){
        ArrayList<Vehiculo> vehiculos=deserializarvehiculos("vehiculos.ser");
        vehiculos.add(this);
        Vehiculo.serializarlistavehiculos(vehiculos,"vehiculos.ser");
        HashMap<String,ArrayList<String>> registros=Util.extraerregistro();
        registros.get(p.getUsuario()).add(this.getplaca());
        Util.guardarregistro(registros);
    }
    public static Vehiculo extraervehiculo(String placa){
        ArrayList<Vehiculo> vehiculos=deserializarvehiculos("vehiculos.ser");
        Vehiculo ve=null;
        for(Vehiculo vehi:vehiculos){
            if(vehi.getplaca().equals(placa))
                ve=vehi;
        }
        return ve;
    }
    public static boolean existe(String placa){
        ArrayList<Vehiculo> vehiculos=Vehiculo.deserializarvehiculos("vehiculos.ser");
        for(Vehiculo v:vehiculos){
            if(v.getplaca().equals(placa))
                return true;
        }
        return false;
    }
    public void eliminarvehiculo(){
        ArrayList<Vehiculo> vehiculos=deserializarvehiculos("vehiculos.ser");
        vehiculos.remove(this);
        Vehiculo.serializarlistavehiculos(vehiculos,"vehiculos.ser");
        HashMap<String,ArrayList<String>> registro=Util.extraerregistro();
        ArrayList<String> placas=new ArrayList<>();
        String p="hola";
        for(HashMap.Entry<String,ArrayList<String>> r:registro.entrySet()){
            if(r.getValue().contains(this.placa)){
                placas=r.getValue();
                p=r.getKey();
                break;
            }
        }
        placas.remove(this.placa);
        registro.put(p,placas);
        Util.guardarregistro(registro);
        ArrayList<Oferta> ofertas=Oferta.deserealizarofertas();
        ArrayList<Oferta> ofertasvalidas=new ArrayList<>();
        for(Oferta o:ofertas){
            if(!o.getVehiculo().equals(this))
                ofertasvalidas.add(o);
        }
        Oferta.serializarlistaofertas(ofertasvalidas);
    }
    public static void guardarplacatemporal(String s){
        try(FileOutputStream f=new FileOutputStream("placatemporal.ser");ObjectOutputStream out=new ObjectOutputStream(f)){
            out.writeObject(s);
        }
        catch (IOException ex) { 
        }
    }public static String extraerplacatemporal(){
        String s=null;
        try(FileInputStream f=new FileInputStream("placatemporal.ser");ObjectInputStream in=new ObjectInputStream(f)){
            s=(String)in.readObject();
        }
        catch (IOException | ClassNotFoundException ex) {
        }
        return s;
    }
    public Persona pertenecea(){
        HashMap<String,ArrayList<String>> registro=Util.extraerregistro();
        String usuario=null;
        for(HashMap.Entry<String,ArrayList<String>> r:registro.entrySet()){
            if(r.getValue().contains(this.placa)){
                usuario=r.getKey();
                break;
            }
        }
        Persona p=Persona.extraerusuario(usuario);
        return p;
    }

    @Override
    public String toString() {
        return "Placa:" + placa +"\n"+ "Marca:" + marca +"\n" +"Modelo:" + modelo +"\n"+ "Motor:" + motor +"\n"+ "Año:" + año+"\n" + "Recorrido:" + recorrido +"\n"+ "Color:" + color+"\n" + "Combustible:" + combustible+"\n" + "Transmision:" + transmision +"\n"+ "Precio:" + precio+"\n";
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null || this.getClass() != o.getClass())
            return false;
        if (this == o)
             return true;
        Vehiculo other = (Vehiculo)o;
        return this.placa.equals(other.placa);
    } 

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.placa);
        hash = 83 * hash + Objects.hashCode(this.marca);
        hash = 83 * hash + Objects.hashCode(this.modelo);
        hash = 83 * hash + Objects.hashCode(this.motor);
        hash = 83 * hash + this.año;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.recorrido) ^ (Double.doubleToLongBits(this.recorrido) >>> 32));
        hash = 83 * hash + Objects.hashCode(this.color);
        hash = 83 * hash + Objects.hashCode(this.combustible);
        hash = 83 * hash + Objects.hashCode(this.transmision);
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.precio) ^ (Double.doubleToLongBits(this.precio) >>> 32));
        hash = 83 * hash + Objects.hashCode(this.vendedor);
        hash = 83 * hash + Objects.hashCode(this.imagen);
        return hash;
    }

    @Override
    public int compareTo(Vehiculo o) {
       if(o.getprecio()<this.getprecio())
            return 1;
        else if(o.getprecio()>this.getprecio()){
            return -1;
        }else
            return 0;
    }
    public static ArrayList<Vehiculo> invertir(ArrayList<Vehiculo> v){
        ArrayList<Vehiculo> v2=new ArrayList<>();
        for(int i=v.size()-1;i>=0;i--)
            v2.add(v.get(i));
        return v2;
    }
    public static ArrayList<Vehiculo> ordenarañoascendente(ArrayList<Vehiculo> v){
        while(true){
            int n=0;
            for(int i=0;i<v.size()-1;i++){
                Vehiculo v1=v.get(i);
                Vehiculo v2=v.get(i+1);
                int a1=v1.getaño();
                int a2=v2.getaño();
                if(a1>a2){
                   v.set(i,v2);
                   v.set(i+1,v1);
                }else
                    n+=1;
            }
            if(n==v.size()-1)
                break;
        }
        return v;
    }
    public static ArrayList<Vehiculo> filtrar(ArrayList<Vehiculo> v,String usuario){
        ArrayList<Vehiculo> ve=new ArrayList<>();
        HashMap<String,ArrayList<String>> r=Util.extraerregistro();
        for(Vehiculo v1:v){
            if(!r.get(usuario).contains(v1.getplaca())){
                ve.add(v1);
            }
        }
        return ve;
    }
}
