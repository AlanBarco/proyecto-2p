module ec.edu.espol.proyectopoo2p {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;               
    requires mail;


    opens ec.edu.espol.proyectopoo2p to javafx.fxml;
    exports ec.edu.espol.proyectopoo2p;
    requires javafx.graphicsEmpty;
}